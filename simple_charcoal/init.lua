-- Loss = cook time + wood
-- Gain = coal - cook time
-- Loss = Gain
-- cook time + wood = coal - cook time
-- 2 * cook time = coal - wood
-- cook time = ( coal - wood ) / 2
-- cook time = ( 40 - 22 ) / 2 = 9 (Aspen)
-- cook time = ( 40 - 30 ) / 2 = 5 (Apple)
-- cook time = ( 40 - 38 ) / 2 = 1 (Jungle)

minetest.register_craft({
	type = "cooking",
	cooktime = 8,
	recipe = "group:tree",
	output = "default:coal_lump",
})
