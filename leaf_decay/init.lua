-- Based on minetes_game/mods/default/functions.lua
-- Modifications by Pedro Gimeno are in the public domain

local function new_after_place_leaves(pos, placer, itemstack, pointed_thing)
  local node = minetest.get_node(pos)
  node.param2 = 0
  minetest.set_node(pos, node)
  local timer = minetest.get_node_timer(pos)
  if not timer:is_started() then
    timer:start(math.random(60, 120))
  end
end

local function call_after_place_leaves(...)
  default.after_place_leaves(...)
end

for k, v in next, minetest.registered_nodes do
  if v.after_place_node == default.after_place_leaves then
    v.after_place_node = call_after_place_leaves
  end
end

default.after_place_leaves = new_after_place_leaves
